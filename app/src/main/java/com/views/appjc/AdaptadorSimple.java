package com.views.appjc;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.appjc.R;

public class AdaptadorSimple extends RecyclerView.Adapter<AdaptadorSimple.MyViewHolder> {
    private String[] mDataset;
    // Provee una referencia a la vista para cada item de datos
    public static class MyViewHolder extends RecyclerView.ViewHolder {
        // cada item de datos es solo una cadena para este ejemplo
        public TextView textView;
        public MyViewHolder(View v) {
            super(v);

            textView = v.findViewById(R.id.textView);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public AdaptadorSimple(String[] myDataset) {
        mDataset = myDataset;
    }

    @Override
    public AdaptadorSimple.MyViewHolder onCreateViewHolder(ViewGroup parent,
                                                           int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.text_row_item_simple, parent, false);

        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.textView.setText(mDataset[position]);
    }

    @Override
    public int getItemCount() {
        return mDataset.length;
    }
}
